defmodule Buh.Email do
  import Bamboo.Email

  def message(from, body) do
    new_email
    |> to("info@buhuchet38.ru")
    |> from("no-reply@buhuchet38.ru")
    |> subject("onsite-message")
    |> text_body("FROM: #{from}\nMESSAGE: #{body}")
  end
end
