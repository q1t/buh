// Brunch automatically concatenates all files in your
// watched paths. Those paths can be configured at
// config.paths.watched in "brunch-config.js".
//
// However, those files will only be executed if
// explicitly imported. The only exception are files
// in vendor, which are never wrapped in imports and
// therefore are always executed.

// Import dependencies
//
// If you no longer want to use a dependency, remember
// to also remove its path from "config.paths.watched".
import "phoenix_html"

// Import local files
//
// Local files can be imported directly using relative
// paths "./socket" or full ones "web/static/js/socket".

// import socket from "./socket"

import * as SS from "smooth-scroll"

const SS_options = {
    selectorHeader: "[data-scroll-header]",
    speed: 500,
    easing: 'easeInOutCubic'
}

SS.init(SS_options)

if (window.location.hash) {
    var anchor = document.querySelector(window.location.hash)
    SS.animateScroll(anchor, null, SS_options);
}
