defmodule Buh.MessageController do
  use Buh.Web, :controller

  def create(conn, %{"email" => email, "message" => message}) do
    if validate_email(email) do
      Buh.Email.message(email, message) |> Buh.Mailer.deliver_later
      conn
      |> put_flash(:info, "Ваше сообщение было отправлено, мы свяжемся с вами в ближайшее время.")
      |> redirect(to: page_path(conn, :index))
    else
      conn
      |> put_flash(:error, "Неверный email")
      |> redirect(to: page_path(conn, :index) <> "#message")
      |> halt
    end
  end

  defp validate_email(email), do: email =~ ~r/@/
end
