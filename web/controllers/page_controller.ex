defmodule Buh.PageController do
  use Buh.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
