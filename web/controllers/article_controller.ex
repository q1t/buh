defmodule Buh.ArticleController do
  use Buh.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end

  def show(conn, %{"title" => title}) do
    render conn, title
  end
end
