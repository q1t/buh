defmodule Buh.Router do
  use Buh.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Buh do
    pipe_through :browser # Use the default browser stack

    get  "/", PageController, :index
    post "/message", MessageController, :create
    get  "/articles", ArticleController, :index
    get  "/articles/:title", ArticleController, :show
  end

  scope "/" do
    if Mix.env == :dev do
      forward "/sent_emails", Bamboo.EmailPreviewPlug
    end
  end
end
