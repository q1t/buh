defmodule Buh.LayoutView do
  use Buh.Web, :view

  def home_page?(Buh.PageView, "index.html"), do: true
  def home_page?(_, _), do: false
end
