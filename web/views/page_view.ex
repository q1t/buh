defmodule Buh.PageView do
  use Buh.Web, :view

  def csrf_form_tag() do
    ~E"""
    <input name="_csrf_token" type="hidden" value="<%= get_csrf_token() %>">
    """
  end
end
